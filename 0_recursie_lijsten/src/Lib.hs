module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Bronnen: http://cheatsheet.codeslower.com/CheatSheet.pdf
-- https://gist.github.com/mikehaertl/3258427
-- https://www.youtube.com/watch?v=02_H3LjqMr8&list=PLhx8v4mr6oU_nAJcsSWe4qrHeON7vG8Ov&index=6&t=01s

-- | Een functie die de som van een lijst getallen berekent
ex1 :: [Int] -> Int
ex1 [] = 0 
ex1 list = (head list) + ex1 (tail list)

-- | Een functie die alle elementen van een lijst met 1 ophoogt.
ex2 :: [Int] -> [Int]
ex2 [] = [] 
ex2 lst = (head lst + 1) : (ex2(tail lst)) 

-- | Een functie die alle elementen van een lijst met -1 vermenigvuldigt.
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 lst = (head lst * (-1)) : (ex3 (tail lst))

-- | Een functie die twee lijsten aan elkaar plakt.
ex4 :: [Int] -> [Int] -> [Int]
ex4 x [] = x
ex4 [] y= y 
ex4 x y = head x : ex4 (tail x) y

-- | Een functie die twee lijsten paarsgewijs bij elkaar optelt.
ex5 :: [Int] -> [Int] -> [Int]
ex5 xs [] = xs 
ex5 [] ys = ys 
ex5 (x:xs) (y:ys) = (x + y) : ex5 xs ys 

-- | Een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt.
ex6 :: [Int] -> [Int] -> [Int]
ex6 xs [] = xs 
ex6 [] ys = ys 
ex6 (x:xs) (y:ys) = (x * y) : ex6 xs ys 

-- | Een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent.
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1 $ ex6 x y